# -*- coding: utf-8 -*-
"""
Created on Tue May  4 17:10:31 2021

@author: gamelina
"""
import numpy as np
from mbtrack2 import BeamLoadingEquilibrium
from mbtrack2.tracking.rf import CavityResonator, RFCavity
from machine_data import v0356
from scipy.constants import c
import matplotlib as mpl
import matplotlib.pyplot as plt

plt.rcParams['figure.figsize'] = [12, 5]
plt.rcParams['figure.dpi'] = 200

mpi_switch = True

Vc = 1.8e6
ring = v0356()
ring.U0 = 4.183751e-01*1e6
ring.L = 354.48130800001866
I0 = 0.5

MC = RFCavity(ring, 1, Vc, np.arccos(ring.U0/Vc))

m = 4
Rs = 90e8
Q = 1e8
QL = 1e8
detune = 140e3
HHC = CavityResonator(ring, m, Rs, Q, QL,detune)
HHC.Vg = 0
HHC.theta_g = 0


import numpy as np
from machine_data import v0356, load_TDR_1_wf
from mbtrack2.tracking import Beam, CavityResonator, LongitudinalMap, SynchrotronRadiation
from mbtrack2.tracking.monitors import  BeamMonitor, CavityMonitor, ProfileMonitor
from mbtrack2 import WakeField, WakePotential, WakePotentialMonitor


long = LongitudinalMap(ring)
rad = SynchrotronRadiation(ring)

buffer = 100
tot_turns = 40e3

name = "benchmark_elegant"

HCmon = CavityMonitor("HHC", ring, file_name=name, save_every=100,
                 buffer_size=buffer, total_size=tot_turns/100, mpi_mode=mpi_switch)

bbmon = BeamMonitor(h=ring.h, file_name=None, save_every=10, buffer_size=buffer,
                 total_size=tot_turns/10, mpi_mode=mpi_switch)

promon0 = ProfileMonitor(bunch_number=0, save_every=100, buffer_size=buffer, total_size=tot_turns/100, 
                 dimensions="tau", n_bin=50, file_name=None, mpi_mode=mpi_switch)

promon100 = ProfileMonitor(bunch_number=100, save_every=100, buffer_size=buffer, total_size=tot_turns/100, 
                 dimensions="tau", n_bin=50, file_name=None, mpi_mode=mpi_switch)

promon200 = ProfileMonitor(bunch_number=200, save_every=100, buffer_size=buffer, total_size=tot_turns/100, 
                 dimensions="tau", n_bin=50, file_name=None, mpi_mode=mpi_switch)

promon300 = ProfileMonitor(bunch_number=300, save_every=100, buffer_size=buffer, total_size=tot_turns/100, 
                 dimensions="tau", n_bin=50, file_name=None, mpi_mode=mpi_switch)

promon400 = ProfileMonitor(bunch_number=400, save_every=100, buffer_size=buffer, total_size=tot_turns/100, 
                 dimensions="tau", n_bin=50, file_name=None, mpi_mode=mpi_switch)


bb = Beam(ring)
filling = np.ones(ring.h)
filling[-10:] = 0
filling = filling*I0/(ring.h-10)
bb.init_beam(filling, mp_per_bunch=1e6, track_alive=False, mpi=mpi_switch)


HHC.init_phasor(bb)

for i in range(int(tot_turns+1)):
    if (i % 1000 == 0):
        if mpi_switch and (bb.mpi.rank == 0):
            print(i)
        else:
            print(i)

    long.track(bb)
    rad.track(bb)

    if mpi_switch:
        bb.mpi.share_distributions(bb)

    MC.track(bb)
    HHC.track(bb)

    bbmon.track(bb)
    HCmon.track(bb, HHC)

    promon0.track(bb)
    promon100.track(bb)
    promon200.track(bb)
    promon300.track(bb)
    promon400.track(bb)

bbmon.close()


