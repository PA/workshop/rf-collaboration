import numpy as np
from machine_data import sls
from mbtrack2.tracking import Beam, CavityResonator, LongitudinalMap, SynchrotronRadiation
from mbtrack2.tracking.monitors import  BeamMonitor, CavityMonitor, BunchSpectrumMonitor, BeamSpectrumMonitor

Vc = 2.2e6
ring = sls()

m = 1
Rs = 12480000
Q = 40000
QL = 13333
detune = -100e3
MC = CavityResonator(ring, m, Rs, Q, QL,detune)

MC.Vc = Vc
MC.theta = np.arccos(ring.U0/MC.Vc)
MC.set_optimal_detune(0.1)
MC.set_generator(0.1)

m = 3
Rs = 1.41e10
Q = 1.6e8
QL = 1.6e8
detune = 55e3
HHC = CavityResonator(ring, m, Rs, Q, QL,detune)
HHC.Vg = 0
HHC.theta_g = 0

long = LongitudinalMap(ring)
rad = SynchrotronRadiation(ring)

turns = int(4e4)
detune = np.array([1000e3, 100e3, 55e3, 50e3, 40e3, 35e3, 30e3, 25e3, 20e3])

tot_turns = int(len(detune)*turns)

name = "SLS_100mA"

MCmon = CavityMonitor("MC", ring, file_name=name, save_every=10,
                 buffer_size=100, total_size=tot_turns/10, mpi_mode=True)

HCmon = CavityMonitor("HHC", ring, file_name=None, save_every=10,
                 buffer_size=100, total_size=tot_turns/10, mpi_mode=True)

bbmon = BeamMonitor(h=ring.h, file_name=None, save_every=10, buffer_size=100, 
                 total_size=tot_turns/10, mpi_mode=True)

beamspec = BeamSpectrumMonitor(ring, save_every=25e3, buffer_size=1, total_size=9, dim="tau", 
                 n_fft=None, file_name=None, mpi_mode=True)

bunchspec = BunchSpectrumMonitor(ring, bunch_number=0, mp_number=1e4, sample_size=1e3, save_every=25e3, 
                 buffer_size=1, total_size=9, dim="tau", n_fft=None, 
                 file_name=None, mpi_mode=True)

for det in detune:

    bb = Beam(ring)
    filling = np.ones(ring.h)*100e-3/ring.h
    bb.init_beam(filling, mp_per_bunch=1e4, track_alive=False, mpi=True)
    
    HHC.detune = det
    
    MC.init_phasor(bb)
    HHC.init_phasor(bb)
    
    for i in range(int(turns+1)):
        if ((bb.mpi.rank == 0) is True) and (i % 1000 == 0):
            print("detune = " + str(det*1e-3))
            print(i)

        long.track(bb)
        rad.track(bb)
        
        bb.mpi.share_distributions(bb)
                
        MC.track(bb)
        HHC.track(bb)
        
        bbmon.track(bb)
        MCmon.track(bb, MC)
        HCmon.track(bb, HHC)
        if (i >= 15e3):
            beamspec.track(bb)
            bunchspec.track(bb)

bbmon.close()
