# -*- coding: utf-8 -*-
"""
SLS parameters script.
Only longitudinal paramters are correct.

@author: Alexis Gamelin
@date: 08/12/2021
"""

import numpy as np
from pathlib import Path
from mbtrack2.tracking import Synchrotron, Optics, Electron, Beam


def sls():
    
    h = 480
    L = 288.0073
    E0 = 2.4e9
    particle = Electron()
    ac = 6.0406e-4
    U0 = 538.784e3
    tau = np.array([0, 0, 4.28364e-3])
    tune = np.array([0, 0, 0])
    emit = np.array([1e-9, 1e-9*0.01])
    sigma_0 = 12.6e-12
    sigma_delta = 8.74e-4
    chro = [0,0]
    
    # mean values
    beta = np.array([1, 1])
    alpha = np.array([0, 0])
    dispersion = np.array([0, 0, 0, 0])
    optics = Optics(local_beta=beta, local_alpha=alpha, 
                      local_dispersion=dispersion)
    
    ring = Synchrotron(h, optics, particle, L=L, E0=E0, ac=ac, U0=U0, tau=tau,
                       emit=emit, tune=tune, sigma_delta=sigma_delta, 
                       sigma_0=sigma_0, chro=chro)
    
    return ring