# -*- coding: utf-8 -*-
"""
Created on Tue May  4 17:10:31 2021

@author: gamelina
"""
import numpy as np
from mbtrack2.vlasov import BeamLoadingVlasov
from mbtrack2.tracking.rf import CavityResonator
from mbtrack2.collective_effects import gaussian_bunch
from machine_data import sls
from scipy.constants import c
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.signal import peak_widths, find_peaks

import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [12, 5]
plt.rcParams['figure.dpi'] = 200

## No IDs and full coupling
## 3 HHC & 1.7 MV

Vc = 2.2e6
ring = sls()

m = 1
Rs = 12480000
Q = 40000
QL = 13333
detune = -100e3
MC = CavityResonator(ring, m, Rs, Q, QL,detune)

MC.Vc = Vc
MC.theta = np.arccos(ring.U0/MC.Vc)
MC.set_optimal_detune(0.1)
MC.set_generator(0.1)

m = 3
Rs = 1.41e10
Q = 1.6e8
QL = 1.6e8
detune = 55e3
HHC = CavityResonator(ring, m, Rs, Q, QL,detune)
HHC.Vg = 0
HHC.theta_g = 0

HHC.detune = 55e3
V = BeamLoadingVlasov(ring,[MC,HHC], 0.1,auto_set_MC_theta=False)
sol = V.beam_equilibrium(plot=True)

HHC.detune = 25e3
V = BeamLoadingVlasov(ring,[MC,HHC], 0.1,auto_set_MC_theta=False)
sol = V.beam_equilibrium(plot=True)

ax = plt.gca()
ax.legend([r"$f_r - f_3 =$ 55 kHz", r"$f_r - f_3 =$ 25 kHz"])
ax.set_ylabel("arb. unit")

plt.tight_layout()
#plt.savefig("HC3_cas0_83_85kHz.jpg",dpi=200)