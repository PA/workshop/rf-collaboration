# -*- coding: utf-8 -*-
"""
SOLEIL synchrotron parameters script.

@author: Alexis Gamelin
@date: 14/01/2020
"""

import numpy as np
from pathlib import Path
from mbtrack2.tracking import Synchrotron, Optics, Electron, Beam

def v0313_v2(IDs="noID", coupling="full", V_RF=None, ADTS=False):
    # Using RF table from 16/02/2021 (L. Nadolski) 
    
    h = 416
    L = 354.7373
    E0 = 2.75e9
    particle = Electron()
    ac = 9.12e-5
    chro = [1.6,1.6]
    tune = np.array([0.2,0.2])
    sigma_0 = 8e-12
    
    if IDs == "noID" and coupling == "full":
        tau = np.array([9.2e-3, 9.3e-3, 11.7e-3])
        emit = np.array([52e-12, 52e-12])
        sigma_delta = 9e-4
        U0 = 515e3
    
    if IDs == "ID1" and coupling == "full":
        tau = np.array([7.1e-3, 7.1e-3, 6.2e-3])
        emit = np.array([39e-12, 39e-12])
        sigma_delta = 8.9e-4
        U0 = 760e3
        
    if IDs == "ID2" and coupling == "full":
        tau = np.array([6.35e-3, 6.35e-3, 5.1e-3])
        emit = np.array([35e-12, 35e-12])
        sigma_delta = 8.8e-4
        U0 = 874e3
        
    if IDs == "noID" and coupling == "no":
        tau = np.array([7.1e-3, 13.2e-3, 11.7e-3])
        emit = np.array([81e-12, 1e-12])
        sigma_delta = 9e-4
        U0 = 515e3
    
    if IDs == "ID1" and coupling == "no":
        tau = np.array([5.6e-3, 8.8e-3, 6.2e-3])
        emit = np.array([64e-12, 1e-12])
        sigma_delta = 8.9e-4
        U0 = 760e3
        
    if IDs == "ID2" and coupling == "no":
        tau = np.array([5.1e-3, 7.6e-3, 5.1e-3])
        emit = np.array([58e-12, 1e-12])
        sigma_delta = 8.8e-4
        U0 = 874e3 
    
    # mean values
    beta = np.array([3.178, 4.197])
    alpha = np.array([0, 0])
    dispersion = np.array([0, 0, 0, 0])
    optics = Optics(local_beta=beta, local_alpha=alpha, 
                      local_dispersion=dispersion)
    
    if ADTS is True:
        # Name format : coef_yx means coef of vertical ADTS as a function of horizontal offset.
        coef_xx = np.array([2.10012957e+07, -1.95066506e+02, -1.87260829e+03,  
                            4.17160112e-02, 0])
        coef_yx = np.array([1.30396188e+07,  1.55792082e+04,  5.80031793e+02,
                            -2.52678776e-01, 0])
        coef_xy = np.array([9.88551670e+03, -2.80640442e-03, 0])
        coef_yy = np.array([1.84297492e+04, 7.24309395e-08, 0])
        
        adts = [coef_xx, coef_yx, coef_xy, coef_yy]
        
    else:
        adts = None

    ring = Synchrotron(h, optics, particle, L=L, E0=E0, ac=ac, U0=U0, tau=tau,
                       emit=emit, tune=tune, sigma_delta=sigma_delta, 
                       sigma_0=sigma_0, chro=chro, adts=adts)
    
    if V_RF is not None:
        tuneS = ring.synchrotron_tune(V_RF)
        ring.sigma_0 = (ring.sigma_delta * np.abs(ring.eta) 
                        / (tuneS * 2 * np.pi * ring. f0))
    
    return ring
