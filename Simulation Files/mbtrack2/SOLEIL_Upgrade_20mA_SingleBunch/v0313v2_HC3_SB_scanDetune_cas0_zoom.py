import numpy as np
from machine_data import v0313_v2
from mbtrack2.tracking import Beam, CavityResonator, LongitudinalMap, SynchrotronRadiation
from mbtrack2.tracking.monitors import  BunchMonitor, CavityMonitor, BunchSpectrumMonitor

Vc = 1.7e6
ring = v0313_v2(IDs="noID", V_RF=Vc)
Itot = 0.02

m = 1
Rs = 19.6e6
Q = 34e3
QL = 6e3
detune = -100e3
MC = CavityResonator(ring, m, Rs, Q, QL,detune)

MC.Vc = Vc
MC.theta = np.arccos(ring.U0/MC.Vc)
MC.set_optimal_detune(Itot)
MC.set_generator(Itot)

m = 3
Rs = 90e8
Q = 1e8
QL = 1e8
detune = 120e3
HHC = CavityResonator(ring, m, Rs, Q, QL,detune)
HHC.Vg = 0
HHC.theta_g = 0

long = LongitudinalMap(ring)
rad = SynchrotronRadiation(ring)

turns = int(7.5e4)
detune = np.array([4e3, 3.7e3, 3.5e3, 3.4e3, 3.3e3, 3.2e3, 3.1e3])

tot_turns = int(len(detune)*turns)

name = "v0313_HC3_SB_scanDetune"

MCmon = CavityMonitor("MC", ring, file_name=name, save_every=10,
                 buffer_size=100, total_size=tot_turns/10, mpi_mode=False)

HCmon = CavityMonitor("HHC", ring, file_name=None, save_every=10,
                 buffer_size=100, total_size=tot_turns/10, mpi_mode=False)

bunchmon = BunchMonitor(bunch_number=0, file_name=None, save_every=10,
                 buffer_size=100, total_size=tot_turns/10, mpi_mode=False)

bunchspec = BunchSpectrumMonitor(ring, bunch_number=0, mp_number=1e4, sample_size=1e3, save_every=30e3,
                 buffer_size=1, total_size=14, dim="tau", n_fft=None,
                 file_name=None, mpi_mode=False)


for det in detune:

    bb = Beam(ring)
    filling = np.zeros(ring.h)
    filling[0] = Itot
    bb.init_beam(filling, mp_per_bunch=1e4, track_alive=False, mpi=False)
    
    HHC.detune = det
    
    MC.init_phasor(bb)
    HHC.init_phasor(bb)
    
    for i in range(int(turns+1)):
        if (i % 100 == 0):
            print("detune = " + str(det*1e-3))
            print(i)

        long.track(bb)
        rad.track(bb)
                
        MC.track(bb)
        HHC.track(bb)
        
        bunchmon.track(bb)
        MCmon.track(bb, MC)
        HCmon.track(bb, HHC)
        if (i >= 15e3):
            bunchspec.track(bb)

bunchmon.close()

